﻿namespace MsSqlToolBelt.DataObjects.Types
{
    /// <summary>
    /// Provides the different fly out types
    /// </summary>
    internal enum FlyOutType
    {
        /// <summary>
        /// The settings fly out
        /// </summary>
        Settings,

        /// <summary>
        /// The data type fly out
        /// </summary>
        DataTypes,

        /// <summary>
        /// The info fly out
        /// </summary>
        Info
    }
}
