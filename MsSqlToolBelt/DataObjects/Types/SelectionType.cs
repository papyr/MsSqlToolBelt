﻿namespace MsSqlToolBelt.DataObjects.Types
{
    /// <summary>
    /// Provides the different selection types
    /// </summary>
    internal enum SelectionType
    {
        /// <summary>
        /// Check all column entries
        /// </summary>
        All,

        /// <summary>
        /// Uncheck all column entries
        /// </summary>
        None
    }
}
