﻿namespace MsSqlToolBelt.DataObjects.Types
{
    /// <summary>
    /// The different code types
    /// </summary>
    public enum CodeType
    {
        /// <summary>
        /// Shows / creates the sql code
        /// </summary>
        Sql,

        /// <summary>
        /// Shows / creates the csharp code
        /// </summary>
        CSharp,

        /// <summary>
        /// Nothing is selected
        /// </summary>
        None
    }
}
